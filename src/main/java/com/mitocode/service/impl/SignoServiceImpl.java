package com.mitocode.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mitocode.model.Paciente;
import com.mitocode.model.SignoVital;
import com.mitocode.repo.IGenericRepo;
import com.mitocode.repo.ISignoRepo;
import com.mitocode.service.ISignoService;

@Service
public class SignoServiceImpl extends CRUDImpl<SignoVital, Integer> implements ISignoService{

	@Autowired
	private ISignoRepo repo;
	
	@Override
	protected IGenericRepo<SignoVital, Integer> getRepo(){
		return repo;
	}
	
	@Override
	public Page<SignoVital> listarPageable(Pageable pageable) {
		return repo.findAll(pageable);
	}

}
